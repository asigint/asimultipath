"""
cc
f_mat
feat_v
ftf_i_ft
"""

from numba import jit, prange
import numpy as np


@jit(parallel=True, nopython=True)
def est_angle_indices(v, f_mat, ftf_i_ft, n_az):
    """
    *** Can't find anything that uses this
    :param v:
    :param f_mat:
    :param ftf_i_ft:
    :param n_az:
    :return:
    """
    n_ap, n_f, n_c = f_mat.shape
    e2 = np.zeros(n_ap)
    c1 = ftf_i_ft.reshape(n_ap * n_c, n_f) @ v
    for m in prange(c1.shape[0] // n_c):
        err = v - f_mat[m] @ c1[m * n_c: (m + 1) * n_c]
        e2[m] = (np.abs(err) ** 2).real.sum()
    index = e2.argmin()
    az_index = index % n_az
    dep_index = index // n_az
    return az_index, dep_index


@jit
def compute_fc(f):
    """

    :param f:
    :return:
    """
    vl = len(f)
    f_new = np.zeros(f.shape)
    f_new[:vl // 2] = -f[vl // 2:]
    f_new[vl // 2:] = f[:vl // 2]
    return f_new


@jit
def feature_map_engine(v1, n_p, f_mat, ftf_ft_i, indices):
    """

    :param v1:
    :param n_p: number of ports
    :param f_mat:
    :param ftf_ft_i:
    :param indices:
    :return:
    """
    n_vec = len(v1) // n_p
    n_feat = n_p * (n_p + 1)
    f_tmp = np.zeros((n_feat, n_vec ** 2))
    # f_mat = np.zeros((n_feat, n_vec ** 2))

    for col in range(n_vec):
        for row in range(n_vec):
            index = col * n_vec + row
            x1 = (v1[row * n_p:(row + 1) * n_p] @ v1[col * n_p: (col + 1) * n_p].conj().T).flatten()[indices]
            f_tmp[:n_feat // 2, index] = x1.real
            f_tmp[n_feat // 2:, index] = x1.imag
    for col in range(n_vec):
        for row in range(col, n_vec):
            index1 = col * n_vec + row
            if col == row:
                f_mat[:, index1] = f_tmp[:, index1]
            else:
                index2 = row * n_vec + col
                f_mat[:, index1] = f_tmp[:, index1] + f_tmp[:, index2]
                f_mat[:, index2] = compute_fc(f_tmp[:, index1] - f_tmp[:, index2])
    # return f_mat
    ftf_ft_i[:] = np.linalg.pinv(f_mat.T @ f_mat) @ f_mat.T


@jit(parallel=True, nopython=True)
def compute_cc_f_map(mv, mh, b1, indices, n_bias_v):
    """

    :param mv: manifold vertical
    :param mh: manifold horizontal
    :param b1: bias
    :param indices:
    :param n_bias_v:
    :return:
    """

    # if b1 is not None:
    #     n_bias_v = b1.shape[-1]
    # else:
    #     n_bias_v = 0

    n_basis_v = 2
    if mv is not None:
        n_dep = mv.shape[0]
        n_az = mv.shape[1]
        n_ports = mv.shape[2]
        if mh is None:
            n_basis_v = 1
    elif mh is not None:
        n_dep = mh.shape[0]
        n_az = mh.shape[1]
        n_ports = mh.shape[2]
        n_basis_v = 1
    else:
        return None, None

    n_cols = (n_basis_v + n_bias_v) ** 2
    n_pos = n_dep * n_az

    f_mat = np.zeros((n_pos, n_ports * (n_ports + 1), n_cols))
    ftf_ft_i = np.zeros((n_pos, n_cols, n_ports * (n_ports + 1)))

    for m in prange(n_pos):
        index = 0
        dep = m // n_az
        az = m % n_az
        v1 = np.zeros(((n_basis_v + n_bias_v) * n_ports, 1), np.complex128)
        if mv is not None:
            v1[index * n_ports: (index + 1) * n_ports, 0] = mv[dep, az]
            index += 1
        if mh is not None:
            v1[index * n_ports: (index + 1) * n_ports, 0] = mh[dep, az]
            index += 1
        for n in range(n_bias_v):
            if b1 is not None:
                v1[(index + n) * n_ports: (index + n + 1) * n_ports, 0] = b1[:, n]
        feature_map_engine(v1, n_ports, f_mat[m], ftf_ft_i[m], indices)
    return f_mat, ftf_ft_i


@jit(parallel=True)
def compute_cc_f_map_with_bias(mv, mh, b1, indices):
    """

    :param mv:
    :param mh:
    :param b1:
    :param indices:
    :return:
    """

    n_dep = mv.shape[0]
    n_az = mv.shape[1]
    n_ports = mv.shape[2]
    n_pos = n_dep * n_az
    n_bias_v = b1.shape[-1]
    n_cols = (2 + n_bias_v) ** 2

    # n_pos, n_ports = mv.shape
    f_mat = np.zeros((n_pos, n_ports * (n_ports + 1), n_cols))
    ftf_ft_i = np.zeros((n_pos, n_cols, n_ports * (n_ports + 1)))

    for m in prange(n_pos):
        dep = m // n_az
        az = m % n_az
        v1 = np.zeros(((2 + n_bias_v) * n_ports, 1), np.complex128)
        v1[0 * n_ports: 1 * n_ports, 0] = mv[dep, az]
        v1[1 * n_ports: 2 * n_ports, 0] = mh[dep, az]
        for n in range(n_bias_v):
            v1[(2 + n) * n_ports: (3 + n) * n_ports, 0] = b1[:, n]
        feature_map_engine(v1, n_ports, f_mat[m], ftf_ft_i[m], indices)
    return f_mat, ftf_ft_i


@jit(parallel=True)
def compute_cc_f_map_no_bias(mv, mh, indices):

    n_dep = mv.shape[0]
    n_az = mv.shape[1]
    n_ports = mv.shape[2]
    n_pos = n_dep * n_az

    f_mat = np.zeros((n_pos, n_ports * (n_ports + 1), 4))
    ftf_ft_i = np.zeros((n_pos, 4, n_ports * (n_ports + 1)))

    for m in prange(n_pos):
        dep = m // n_az
        az = m % n_az
        v1 = np.zeros((2 * n_ports, 1), np.complex128)
        v1[0 * n_ports: 1 * n_ports, 0] = mv[dep, az]
        v1[1 * n_ports: 2 * n_ports, 0] = mh[dep, az]
        feature_map_engine(v1, n_ports, f_mat[m], ftf_ft_i[m], indices)
    return f_mat, ftf_ft_i


@jit(parallel=True)
def get_projection_error(feat_v, f_mat, ftf_i_ft):

    n_candidates = f_mat.shape[0]
    ss_mat = np.zeros((n_candidates, feat_v.shape[1]))
    c = np.zeros((n_candidates, f_mat.shape[2], 1), feat_v.dtype)
    for m in prange(n_candidates):
        c[m] = ftf_i_ft[m] @ feat_v
        e = feat_v - f_mat[m] @ c[m]
        ss_mat[m] = (e.conj().T @ e).real
    return ss_mat, c


@jit(parallel=True)
def get_ftf_i_ft(f_mat):

    n_pos = f_mat.shape[0]
    n_feats = f_mat.shape[1]
    n_cols = f_mat.shape[2]
    ftf_i_ft = np.zeros((n_pos, n_cols, n_feats), f_mat.dtype)

    for m in prange(n_pos):
        ftf_i_ft[m] = np.linalg.pinv(f_mat[m].conj().T @ f_mat[m]) @ f_mat[m].conj().T

    return ftf_i_ft


@jit(parallel=True)
def compute_v_f_mat_no_bias(mv, mh):

    if mv is not None and mh is not None:
        dep = mv.shape[0]
        az = mv.shape[1]
        f_mat = np.zeros((dep * az, 2 * mv.shape[-1], 4))
        ftf_i_ft = np.zeros((dep * az, 4, 2 * mv.shape[-1]))
    elif mv is not None:
        dep = mv.shape[0]
        az = mv.shape[1]
        f_mat = np.zeros((dep * az, 2 * mv.shape[-1], 2))
        ftf_i_ft = np.zeros((dep * az, 2, 2 * mv.shape[-1]))
    elif mh is not None:
        dep = mh.shape[0]
        az = mh.shape[1]
        f_mat = np.zeros((dep * az, 2 * mh.shape[-1], 2))
        ftf_i_ft = np.zeros((dep * az, 2, 2 * mh.shape[-1]))
    else:
        return None, None

    for m in prange(dep):
        for n in range(az):
            index = 0
            pos = m * az + n
            if mv is not None:
                f_mat[pos, :mv.shape[-1], index] = mv[m, n].real
                f_mat[pos, mv.shape[-1]:, index] = mv[m, n].imag
                index += 1
                f_mat[pos, :, index] = compute_fc(f_mat[pos, :, index - 1])
                index += 1
            if mh is not None:
                f_mat[pos, :mh.shape[-1], index] = mh[m, n].real
                f_mat[pos, mh.shape[-1]:, index] = mh[m, n].imag
                index += 1
                f_mat[pos, :, index] = compute_fc(f_mat[pos, :, index - 1])
            ftf_i_ft[pos] = np.linalg.pinv(f_mat[pos].conj().T @ f_mat[pos]) @ f_mat[pos].conj().T

    return f_mat, ftf_i_ft


@jit(parallel=True)
def compute_v_f_mat_with_bias(mv, mh, b):

    dep = mv.shape[0]
    az = mv.shape[1]
    n_bias_v = b.shape[-1]

    f_mat = np.zeros((dep * az, 2 * mv.shape[-1], (4 + n_bias_v)))
    ftf_i_ft = np.zeros((dep * az, (4 + n_bias_v), 2 * mv.shape[-1]))

    for m in prange(dep):
        for n in range(az):
            pos = m * az + n
            f_mat[pos, :mv.shape[-1], 0] = mv[m, n].real
            f_mat[pos, mv.shape[-1]:, 0] = mv[m, n].imag
            f_mat[pos, :, 1] = compute_fc(f_mat[pos, :, 0])
            f_mat[pos, :mh.shape[-1], 2] = mh[m, n].real
            f_mat[pos, mh.shape[-1]:, 2] = mh[m, n].imag
            f_mat[pos, :, 3] = compute_fc(f_mat[pos, :, 2])
            for k in range(n_bias_v):
                f_mat[pos, :mv.shape[-1], 4 + k] = b[:, k].real
                f_mat[pos, mv.shape[-1]:, 4 + k] = b[:, k].imag
            ftf_i_ft[pos] = np.linalg.pinv(f_mat[pos].conj().T @ f_mat[pos]) @ f_mat[pos].conj().T

    return f_mat, ftf_i_ft


@jit(parallel=True, nopython=True)
def compute_v_f_mat(mv, mh, b, n_bias_v):

    # if b is None:
    #     n_bias_v = 0
    # else:
    #     n_bias_v = b.shape[-1]

    if mv is not None and mh is not None:
        dep = mv.shape[0]
        az = mv.shape[1]
        f_mat = np.zeros((dep * az, 2 * mv.shape[-1], (4 + n_bias_v)))
        ftf_i_ft = np.zeros((dep * az, (4 + n_bias_v), 2 * mv.shape[-1]))
    elif mv is not None:
        dep = mv.shape[0]
        az = mv.shape[1]
        f_mat = np.zeros((dep * az, 2 * mv.shape[-1], 2 + n_bias_v))
        ftf_i_ft = np.zeros((dep * az, 2 + n_bias_v, 2 * mv.shape[-1]))
    elif mh is not None:
        dep = mh.shape[0]
        az = mh.shape[1]
        f_mat = np.zeros((dep * az, 2 * mh.shape[-1], 2 + n_bias_v))
        ftf_i_ft = np.zeros((dep * az, 2 + n_bias_v, 2 * mh.shape[-1]))
    else:
        return None, None

    for m in prange(dep):
        for n in range(az):
            index = 0
            pos = m * az + n
            if mv is not None:
                f_mat[pos, :mv.shape[-1], index] = mv[m, n].real
                f_mat[pos, mv.shape[-1]:, index] = mv[m, n].imag
                index += 1
                f_mat[pos, :, index] = compute_fc(f_mat[pos, :, index - 1])
                index += 1
            if mh is not None:
                f_mat[pos, :mh.shape[-1], index] = mh[m, n].real
                f_mat[pos, mh.shape[-1]:, index] = mh[m, n].imag
                index += 1
                f_mat[pos, :, index] = compute_fc(f_mat[pos, :, index - 1])
                index += 1
            for k in range(n_bias_v):
                if b is not None:
                    f_mat[pos, :f_mat.shape[1] // 2, index + k] = b[:, k].real
                    f_mat[pos, f_mat.shape[1] // 2:, index + k] = b[:, k].imag
            ftf_i_ft[pos] = np.linalg.pinv(f_mat[pos].conj().T @ f_mat[pos]) @ f_mat[pos].conj().T

    return f_mat, ftf_i_ft


@jit(nopython=True)
def man_interp_fast(manifold1, freq1, manifold2, freq2, manifold_out, freq):

    rows = manifold1.shape[0]
    cols = manifold1.shape[1]

    freq_v = np.array([freq1, freq2])
    for m in range(rows):
        for n in range(cols):
            manifold = np.array([manifold1[m, n], manifold2[m, n]])
            manifold_out[m, n] = np.interp(freq, freq_v, manifold)


def interpolate_manifold(manifold_data, frequency, dep_indices=None, az_indices=None):

    basis = manifold_data['basis']
    if dep_indices is None:
        dep_indices = np.arange(len(manifold_data['cal_depression']))
    if az_indices is None:
        az_indices = np.arange(len(manifold_data['cal_azimuth']))

    if not isinstance(dep_indices, (list, np.ndarray)):
        dep_indices = [dep_indices]
    if not isinstance(az_indices, (list, np.ndarray)):
        az_indices = [az_indices]

    n_ports = manifold_data['number_ports']
    if 'v_pol' in basis:
        v_int = basis['v_pol']
        mv = np.zeros((len(dep_indices), len(az_indices), n_ports), np.complex128)
    else:
        v_int = None
        mv = None

    if 'h_pol' in basis:
        h_int = basis['h_pol']
        mh = np.zeros((len(dep_indices), len(az_indices), n_ports), np.complex128)
    else:
        h_int = None
        mh = None

    for m, dep in enumerate(dep_indices):
        for n, az in enumerate(az_indices):
            if mv is not None:
                mv[m, n].real = v_int[dep, az]['w_r'](frequency)
                mv[m, n].imag = v_int[dep, az]['w_i'](frequency)
            if mh is not None:
                mh[m, n].real = h_int[dep, az]['w_r'](frequency)
                mh[m, n].imag = h_int[dep, az]['w_i'](frequency)

    return mv, mh


def interpolate_manifold_2_pt(manifold_data, frequency, dep_indices=None, az_indices=None):
    basis = manifold_data['basis']
    if dep_indices is None:
        dep_indices = np.arange(len(manifold_data['cal_depression']))
    if az_indices is None:
        az_indices = np.arange(len(manifold_data['cal_azimuth']))

    if not isinstance(dep_indices, (list, np.ndarray)):
        dep_indices = [dep_indices]
    if not isinstance(az_indices, (list, np.ndarray)):
        az_indices = [az_indices]

    # Frame the manifold between 2 pre-computed frequencies
    manifold_data['cal_frequencies'] = np.asarray(manifold_data['cal_frequencies'])
    i_low = np.where(manifold_data['cal_frequencies'] <= frequency)[0]
    if len(i_low) > 0:
        i_low = i_low[-1]
    else:
        i_low = 0
    f_low = manifold_data['cal_frequencies'][i_low]

    i_high = np.where(manifold_data['cal_frequencies'] >= frequency)[0]
    if len(i_high) > 0:
        i_high = i_high[0]
    else:
        i_high = len(manifold_data['cal_frequencies']) - 1
    f_high = manifold_data['cal_frequencies'][i_high]

    # Allocate storage
    n_ports = manifold_data['number_ports']
    if 'v_pol' in basis:
        v_man = basis['v_pol']
        mv = np.zeros((len(dep_indices), len(az_indices), n_ports), np.complex128)
    else:
        v_man = None
        mv = None

    if 'h_pol' in basis:
        h_man = basis['h_pol']
        mh = np.zeros((len(dep_indices), len(az_indices), n_ports), np.complex128)
    else:
        h_man = None
        mh = None

    for p in range(n_ports):
        if v_man is not None:
            v_man_l = v_man[i_low, dep_indices][:, az_indices, p]
            v_man_h = v_man[i_high, dep_indices][:, az_indices, p]
            man_interp_fast(v_man_l, f_low, v_man_h, f_high, mv[..., p], frequency)
        if h_man is not None:
            h_man_l = h_man[i_low, dep_indices][:, az_indices, p]
            h_man_h = h_man[i_high, dep_indices][:, az_indices, p]
            man_interp_fast(h_man_l, f_low, h_man_h, f_high, mh[..., p], frequency)

    return mv, mh


def construct_voltage_basis(manifold_data, frequency, bias_v=None, dep_indices=None, az_indices=None):

    n_bias_v = 0
    if bias_v is not None:
        bias = bias_v
    elif 'bias_v' in manifold_data and manifold_data['bias_v'] is not None:
        bias = manifold_data['bias_v'](frequency)
    else:
        bias = None

    if bias is not None and len(bias.shape) == 1:
        bias = bias.reshape(bias.shape[0], 1)

    if bias is not None:
        n_bias_v = bias.shape[-1]

    if 'v_pol' in manifold_data['basis']:
        n_dim = len(manifold_data['basis']['v_pol'].shape)
    else:
        n_dim = len(manifold_data['basis']['h_pol'].shape)

    if n_dim == 2:
        mv, mh = interpolate_manifold(manifold_data, frequency, dep_indices, az_indices)
    else:
        mv, mh = interpolate_manifold_2_pt(manifold_data, frequency, dep_indices, az_indices)

    f_mat, ftf_i_ft = compute_v_f_mat(mv, mh, bias, n_bias_v)

    return f_mat, ftf_i_ft


def construct_cc_basis(manifold_data, frequency, bias_v=None, dep_indices=None, az_indices=None):
    """

    :param manifold_data:
    :param frequency:
    :param bias_v:
    :param dep_indices:
    :param az_indices:
    :return:
    """

    n_bias_v = 0
    if bias_v is not None:
        bias = bias_v
    elif 'bias_v' in manifold_data:
        bias = manifold_data['bias_v'](frequency)
    else:
        bias = None

    if bias is not None and len(bias.shape) == 1:
        bias = bias.reshape(bias.shape[0], 1)

    if bias is not None:
        n_bias_v = bias.shape[-1]

    if 'v_pol' in manifold_data['basis']:
        n_dim = len(manifold_data['basis']['v_pol'].shape)
    else:
        n_dim = len(manifold_data['basis']['h_pol'].shape)

    if n_dim == 2:
        mv, mh = interpolate_manifold(manifold_data, frequency, dep_indices, az_indices)
    else:
        mv, mh = interpolate_manifold_2_pt(manifold_data, frequency, dep_indices, az_indices)

    n_ports = manifold_data['number_ports']
    indices = np.triu_indices(n_ports)
    indices = indices[0] * n_ports + indices[1]

    f_mat, ftf_i_ft = compute_cc_f_map(mv, mh, bias, indices, n_bias_v)

    return f_mat, ftf_i_ft
