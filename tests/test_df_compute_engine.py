from AsiDfEngine.AsiDfE import AsiDfE
# import AsiDfEngine.df_compute_engine as dfce
from asimultipath.lib import df_compute_engine as dfce
import numpy as np


if __name__ == '__main__':
    # test that talks directly to compute engine
    # this uses whatever manifold in test_settings/manifolds
    dfe = AsiDfE(configuration_path='../test_settings', configuration_file='df_service.yml')
    manifold_data = dfe.sensor_array['array_object'][0].manifold_data

    # voltage
    fm, ftf_i_ft = dfce.construct_voltage_basis(manifold_data, 122e6)
    print(f'\nvoltage fm.shape={fm.shape}')
    print(f'voltage ftf_i_ft.shape={ftf_i_ft.shape}')
    # print(f'\nvoltage ftf_i_ft=\n{ftf_i_ft}')

    # cc
    fm, ftf_i_ft = dfce.construct_cc_basis(manifold_data, 122e6)
    print(f'\ncc fm.shape={fm.shape}')
    print(f'cc ftf_i_ft.shape={ftf_i_ft.shape}')
    # print(f'\ncc ftf_i_ft=\n{ftf_i_ft}')

