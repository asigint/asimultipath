from AsiDfEngine.AsiDfE import AsiDfE
# import AsiDfEngine.df_compute_engine as dfce
from asimultipath.lib import df_compute_engine as dfce
import numpy as np


if __name__ == '__main__':

    n_ports = 4
    print(f'\nn_ports={n_ports}')

    # Make test vector
    np.random.seed(0)
    test_v = np.random.randn(n_ports, 1) + 1j * np.random.randn(n_ports, 1)
    print(f'\ntest vector=\n{test_v}')

    # Compute cc feature vector
    cc = test_v @ test_v.conj().T
    f_v = cc[np.triu_indices(n_ports)]
    f_v = f_v.reshape(len(f_v), 1)

    # Test cc manifold
    meta_d = {}
    # dfe = AsiDfE(configuration_path='/opt/asi/don/settings', configuration_file='df_service.yml')
    dfe = AsiDfE(configuration_path='../test_settings', configuration_file='df_service.yml')
    df_dict = dfe.get_df_results(freq=122e6, meta_data=meta_d, v=f_v)
    print(f'\nTest cc manifold\ndf_dict["aoa"]={df_dict["aoa"]}  df_dict["dep"]={df_dict["dep"]}')

    # Test voltage manifold
    # dfe = AsiDfE(configuration_path='/opt/asi/don/settings', configuration_file='df_service.yml')
    dfe = AsiDfE(configuration_path='../test_settings', configuration_file='df_service.yml')
    dfe.sensor_array['array_object'][0].array_parameters['reference_active'] = True
    df_dict2 = dfe.get_df_results(freq=122e6, meta_data=meta_d, v=test_v)
    print(f'\nTest voltage manifold...\ndf_dict["aoa"]={df_dict2["aoa"]}  df_dict["dep"]={df_dict2["dep"]}')

    # Alternative test that talks directly to compute engine
    # this uses whatever manifold I have in test_settings/manifolds
    manifold_data = dfe.sensor_array['array_object'][0].manifold_data

    # Voltage
    fm, ftf_i_ft = dfce.construct_voltage_basis(manifold_data, 122e6)
    print(f'\nvoltage fm.shape={fm.shape}')
    print(f'voltage ftf_i_ft.shape={ftf_i_ft.shape}')
    # print(f'\nvoltage ftf_i_ft=\n{ftf_i_ft}')

    # CC
    fm, ftf_i_ft = dfce.construct_cc_basis(manifold_data, 122e6)
    print(f'\ncc fm.shape={fm.shape}')
    print(f'cc ftf_i_ft.shape={ftf_i_ft.shape}')
    # print(f'\ncc ftf_i_ft=\n{ftf_i_ft}')
